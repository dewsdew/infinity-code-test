const firstFunction = new Promise((resolve, reject) => {
    setTimeout(() => {
        console.log('1')
        resolve()
}, 3000)
}) 

const secondFunction = () => {
    setTimeout(() => {
        console.log('2')
    }, 1000)
}

const thirdFunction = () => {
    setTimeout(() => {
        console.log('3')
    }, 2000)
}

firstFunction.then(() => {
    secondFunction()
    thirdFunction()
})



// โจทย์ข้อสุดท้ายให้ใช้ promise ในการช่วยให้ console.log ออกมาเรียงลำดับ 1 , 2 , 3 ตามลำดับ
// โดยไม่อนุญาติให้แก้เลขเวลาใน setTimeout โดยให้ เรียก firstFunction ก่อน หลังจากนั้นจึงตามด้วย
// secondFunction และ thirdFunction ตามลำดับ

// firstFunction()
// secondFunction()
// thirdFunction()
