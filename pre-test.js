const data = require('./example-data.json')
let checkNum = n => isNaN(n) ? 0 : n

//1
let getProductById =  data.find(p => p.id === 10207).name

//2
let getAllProductsPrice = data.map(product => product.price).reduce((val, cur) => checkNum(val) + checkNum(cur), 0)

//3
let getIsAllVat0 = data.every(product => product.vat_percent === 0)

//4
let getSubProductsLT200 = data.filter(product => product.products.find(item => item.price <= 200)).map(res => res.name)

//5
data.map(product => product.isToggle = false)
let getProductIsToggle = data.map(res => {
    return {name: res.name, isToggle: res.isToggle}
})

//6
data.map(product => product.productsTotalPrice = product.products.reduce((val, cur) => checkNum(val) + checkNum(cur.price), 0))
let getProductsTotalPrice = data.map(res => {
    return {name: res.name, productsTotalPrice: res.productsTotalPrice}
})

//7
let filProductIsEdit = data.filter(product => !product.is_editable_price)
filProductIsEdit.map(item => item.totalSubProductWeight = item.products.reduce((val, cur) => checkNum(val) + checkNum(cur.weight), 0));
let getProductsTotalWeight = filProductIsEdit.map(res => {
    return {name: res.name, totalSubProductWeight: res.totalSubProductWeight}
})

//8
let getIndexProductWithType = data.findIndex(product => product.products.find(t => t.type && t.type.length))

//9
let getProductsWithIsType = []
data.forEach(product => {
    if(product.products && product.products.every(item => item.type && item.type.length)){
        product.products.forEach(res => {
            res.type.forEach(t => {
                getProductsWithIsType.push(t.id)
            })
        })
    }
})

console.log(`ข้อ 1 -->`, getProductById)
console.log(`=================================================`)
console.log(`ข้อ 2 -->`, getAllProductsPrice)
console.log(`=================================================`)
console.log(`ข้อ 3 -->`, getIsAllVat0)
console.log(`=================================================`)
console.log(`ข้อ 4 -->`, getSubProductsLT200)
console.log(`=================================================`)
console.log(`ข้อ 5 -->`, getProductIsToggle)
console.log(`=================================================`)
console.log(`ข้อ 6 -->`, getProductsTotalPrice)
console.log(`=================================================`)
console.log(`ข้อ 7 -->`, getProductsTotalWeight)
console.log(`=================================================`)
console.log(`ข้อ 8 -->`, getIndexProductWithType)
console.log(`=================================================`)
console.log(`ข้อ 9 -->`, getProductsWithIsType)
console.log(`=================================================`)


